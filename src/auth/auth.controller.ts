import { Controller, Inject, Post, Get, Body, UsePipes, ValidationPipe } from '@nestjs/common';
import { Routes, Services } from 'src/utils/constants';
import { IAuthService } from './auth';
import CreateUserDto from './dtos/createuser.dto';
import { IUserService } from 'src/users/user';
@Controller(Routes.AUTH)
export class AuthController {
    constructor(
        @Inject(Services.AUTH) private authService: IAuthService,
        @Inject(Services.USERS) private userService: IUserService
    ) { }

    @Post('register')
    @UsePipes(ValidationPipe)
    registerUser(@Body() createUserDto: CreateUserDto) {
        console.log(createUserDto)
        this.userService.createUser(createUserDto)
    }

    @Post('login')
    login() {}

    @Get('status')
    status() {}

    @Get('logout')
    logout() {}

}
