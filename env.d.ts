declare namespace NodeJS {
    export interface ProcessEnv {
        MYSQL_HOST: string
        MYSQL_PORT: number
        MYSQL_USERNAME: string
        MYSQL_PASSWORD: string
        MYSQL_DATABASE: string
    }
}